<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('encryption'));
	}

	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_userdata('csrfkey', $key);
		$this->session->set_userdata('csrfvalue', $value);

		return array($key => $value);
	}

	public function decode_uri()
	{
		$data = $this->uri->segment(4);
		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
	}


	public function get_by_id($table, $id)
	{
		$this->db->from($table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update($table, $where, $data)
	{
		$this->db->update($table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($table, $id)
	{
		$this->db->where('id', $id);
		$this->db->delete($table);
	}

	public function get_all($table)
	{
		$this->db->select('id, name');
		return $this->db->get($table)->result();
	}

	public function _do_upload($path)
	{
		if(empty($path))
		{
			$path = 'upload/';
		}

		$config['upload_path']          = $path;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 200; //set max size allowed in Kilobyte
        $config['max_width']            = 1000; // set max width image allowed
        $config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
            $data['inputerror'][] = 'photo';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	public function _valid_csrf_nonce()
	{
		$csrfkey = $this->input->post($this->session->userdata('csrfkey'));
		if ($csrfkey && $csrfkey == $this->session->userdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function send_mail($from, $to, $subject, $content)
	{
		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'ssl://srv33.niagahoster.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'webmaster@bikinaplikasiweb.com',
		    'smtp_pass' => 'webmaster@fire.120',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
		);

		$this->load->library('email', $config);

		$this->email->from($from, $from);
		$this->email->to($to);
		
		$this->email->subject($subject);
		$this->email->message($content);
		
		$this->email->send();
		

		echo 'success';
	}

}

/* End of file general_model.php */
/* Location: ./application/models/general_model.php */