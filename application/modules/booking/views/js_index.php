<script type="text/javascript">
	$(function() {
		var date = new Date();
		
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

		var data_events = [];
		reload_calendar(data_events);

		$('[name="area_id"]').change(function(event) {
			var area = $(this).val();
			$.post("<?=site_url('booking/get_rooms')?>", {area: area}, function(resp, textStatus, xhr) {
				$('[name="room_id"]').html(resp);
				$('[name="area_id"]').val(area);
			});
		});

		$('[name="room_id"]').change(function(event) {
				var room = $(this).val();
				$('[name="room_id"]').val(room);
		});

		$('#form-filter').submit(function(event) {
			event.preventDefault();
			$('#btn-filter').addClass('disabled');
			$('#btn-filter').text('Loading data..');
			var data_post = $(this).serialize();
			$.getJSON("<?=site_url('booking/get_events')?>", data_post, function(json, textStatus) {
				$('#btn-filter').removeClass('disabled');
				$('#btn-filter').text('Filter');
				reload_calendar(json);

			});

			
		});

		//daterangepicker
		 $('.input-daterange').daterangepicker({
		 	'minDate':moment(),
		 	'parentEl': "#modal-form",
            'autoUpdateInput': false,
            'applyClass' : 'btn-sm btn-success',
            'cancelClass' : 'btn-sm btn-default',
            "timePicker": true,
            "timePicker24Hour": true,
            locale: {
                applyLabel  : 'Apply',
                cancelLabel : 'Cancel',
            }
        })

	    $('.input-daterange').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('YYYY-MM-DD h:mm:ss') + ' - ' + picker.endDate.format('YYYY-MM-DD h:mm:ss'));
	    });

	    $('.input-daterange').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	    });




	});

	function reload_calendar(data_events) 
	{
		$('#calendar').fullCalendar('destroy');

		var calendar = $('#calendar').fullCalendar({
			buttonHtml: {
				prev: '<i class="ace-icon fa fa-chevron-left"></i>',
				next: '<i class="ace-icon fa fa-chevron-right"></i>'
			},
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			events: data_events,
			editable: true,
			selectable: true,
			selectHelper: true,
			select: function(start, end, allDay) {
				if(start.isBefore(moment())) 
				{
					calendar.fullCalendar('unselect');
					return false;
				}
				else
				{
					add_event()
				}
				
			}
			,
			eventClick: function(calEvent, jsEvent, view) {
				edit_event(calEvent.id)
		}
	});


	}


	function add_event()
	{
	    save_method = 'add';
	    reset_form();
	    // $('#form-event')[0].reset(); // reset form on modals
	    $('#msg').html('');
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string
	    $('#modal-form').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Add Event'); // Set Title to Bootstrap modal title

	}

	function reset_form()
	{
        $('[name="title"]').val('');
        $('[name="event_date"]').val('');
        $('[name="desc"]').val('');
        $('[name="qty"]').val('');
        $('[name="notes"]').val('');
        $('[name="qty"]').val('');
	}

	function edit_event(id)
	{
	    save_method = 'update';
	    reset_form()
	    // $('#form-event')[0].reset(); // reset form on modals
	    $('#msg').html('');
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string

	    $.ajax({
	        url : "<?php echo site_url('booking/ajax_edit_event')?>/" + id,
	        type: "GET",
	        dataType: "JSON",
	        success: function(data)
	        {
	        	if(data.is_allow == 'true')
	        	{
		        	$('[name="id"]').val(data.id);
		            $('[name="area_id"]').val(data.area_id);

		            $('[name="title"]').val(data.title);
		            $('[name="event_date"]').data('daterangepicker').setStartDate(data.start_date);
					$('[name="event_date"]').data('daterangepicker').setEndDate(data.end_date);
		            $('[name="event_date"]').val(data.event_date);
		            $('[name="desc"]').val(data.desc);
		            $('[name="qty"]').val(data.qty);
		            $('[name="notes"]').val(data.notes);
		            $('[name="qty"]').val(data.qty);
		            $('#modal-form').modal('show'); // show bootstrap modal when complete loaded
		            $('.modal-title').text('Edit Event'); // Set title to Bootstrap modal title
		            
		            $('[name="room_id"]').val(data.room_id);
	        	}
	        	else
	        	{
	        		$('.area_id').text( $('[name="area_id"]').find(":selected").text() );
	                $('.room_id').text( $('[name="room_id"]').find(":selected").text() );
	                $('.title').text(data.title);
	                $('.event_date').text(data.event_date)
	                $('.desc').text(data.desc)
	                $('.qty').text(data.qty)
	                $('.notes').text(data.notes)
	                $('.qty').text(data.qty)
	                $('#modal-form-view').modal('show'); // show bootstrap modal when complete loaded
	                $('#modal-form-view .modal-title').text('View Event'); // Set title to Bootstrap modal title
	                
	                $('[name="room_id"]').val(data.room_id);
	        	}

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error get data from ajax');
	        }
	    });
	}


	function save_event()
	{
	    $('#btnSave').text('saving...'); //change button text
	    $('#btnSave').attr('disabled',true); //set button disable 
	    var url;

	    if(save_method == 'add') {
	        url = "<?php echo site_url('booking/ajax_add_event')?>";
	    } else {
	        url = "<?php echo site_url('booking/ajax_update_event')?>";
	    }

	    // ajax adding data to database

	    var formData = new FormData($('#form-event')[0]);
	    $.ajax({
	        url : url,
	        type: "POST",
	        data: formData,
	        contentType: false,
	        processData: false,
	        dataType: "JSON",
	        success: function(data)
	        {

	            if(data.status) //if success close modal and reload ajax table
	            {
	                $('#modal-form').modal('hide');
	                $('#form-filter').trigger('submit');
	            }
	            else
	            {
	                if(typeof data.msg !== 'undefined')
	                {
	                    $('#msg').html(data.msg+'<hr>');
	                }
	                
	                if(typeof data.inputerror !== 'undefined')
	                {

	                    for (var i = 0; i < data.inputerror.length; i++) 
	                    {
	                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
	                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
	                    }
	                }
	            }
	            $('#btnSave').text('save'); //change button text
	            $('#btnSave').attr('disabled',false); //set button enable 


	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error adding / update data');
	            $('#btnSave').text('save'); //change button text
	            $('#btnSave').attr('disabled',false); //set button enable 

	        }
	    });
	}

	function delete_event()
	{
		var id = $('[name="id"]').val();
		if(id == null || id == '')
		{
			alert('Error deleting data');
		}
		else
		{
		    if(confirm('Are you sure delete this data?'))
		    {
		        // ajax delete data to database
		        $.ajax({
		            url : "<?php echo site_url('booking/ajax_delete_event')?>/"+id,
		            type: "POST",
		            dataType: "JSON",
		            success: function(data)
		            {
		                //if success reload ajax table
		                $('#modal-form').modal('hide');
		                $('#form-filter').trigger('submit');
		            },
		            error: function (jqXHR, textStatus, errorThrown)
		            {
		                alert('Error deleting data');
		            }
		        });

		    }
		}
	}

</script>