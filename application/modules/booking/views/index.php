
<div class="page-content" style="background:#FFFFF;">
	<div class="page-header">
		<h1>
			Home
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				Booking
			</small>
		</h1>
	</div><!-- /.page-header -->

	<div class="row">

		<div class="panel panel-default">
			<div class="panel-heading">
				<form id="form-filter" class="form-horizontal">
							<div class="form-group">
								<label for="username" class="col-sm-2 control-label">Area Name</label>
								<div class="col-sm-10">
									<select name="area_id" class="form-control">
										<option value="">--select area--</option>
										<?php 
											foreach ($areas as $area) 
											{
												echo '<option value="'.$area->id.'"> '.$area->name.' </option>';
											}

										 ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="username" class="col-sm-2 control-label">Room Name</label>
								<div class="col-sm-10">
									<select name="room_id" class="form-control">
										<option value="">--select room--</option>
									</select>
								</div>
							</div>
							  
							<div class="form-group">
								<label for="LastName" class="col-sm-2 control-label"></label>
								<div class="col-sm-10">
									<button type="submit" id="btn-filter" class="btn btn-primary">Filter</button>
								</div>
							</div>
						</form>
			</div>
			<div class="panel-body">
				<div class="space"></div>
				<div id="calendar"></div>
			</div>
		</div>

	</div>

<!-- Bootstrap modal -->

<div class="modal fade" id="modal-form">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div id="msg" class="text-danger text-center"></div>

            	<?=form_open('#', array('id'=>'form-event', 'class'=>'form-horizontal') ); ?>
               	<?=form_hidden($csrf); ?>
               	 <input type="hidden" value="" name="id"/> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Area</label>
                        <div class="col-sm-9">
                            <select name="area_id" class="form-control">
								<option value="">--select area--</option>
								<?php 
									foreach ($areas as $area) 
									{
										echo '<option value="'.$area->id.'"> '.$area->name.' </option>';
									}

								 ?>
							</select>
							<span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Room</label>
                        <div class="col-sm-9">
                        	<select name="room_id" class="form-control">
								<option value="">--select room--</option>
							</select>
							<span class="help-block"></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="title" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Date Time</label>
                        <div class="col-sm-9">
	                        <input name="event_date" class="form-control input-daterange" type="text">
	                        <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Desc</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" cols="20" rows="5" name="desc"></textarea>
                              <span class="help-block"></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Qty</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="qty" type="text">
                              <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Notes</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="notes" type="text">
                              <span class="help-block"></span>
                        </div>
                    </div>
                     
                </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="btnSave" onclick="save_event()" class="btn btn-primary">Save changes</button>
				<button type="button" onclick="delete_event()" class="btn btn-danger pull-left">Delete</button>
			</div>
		</div>
	</div>
</div>
<!-- End Bootstrap modal -->
 <!-- Bootstrap modal -->

      <div class="modal fade" id="modal-form-view">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              <form action="" class="form-horizontal">
              <div class="form-group">
                <label class="col-sm-3 control-label">Area</label>
                <div class="col-sm-9">
                  <span class="form-control area_id"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Room</label>
                <div class="col-sm-9">
                  <span class="form-control room_id"></span>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                  <span class="form-control title"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Date Time</label>
                <div class="col-sm-9">
                  <span class="form-control event_date"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Desc</label>
                <div class="col-sm-9">
                  <span class="form-control desc"></span>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Qty</label>
                <div class="col-sm-9">
                  <span class="form-control qty"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Notes</label>
                <div class="col-sm-9">
                  <span class="form-control notes"></span>
                </div>
              </div>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- End Bootstrap modal -->  				

</div><!-- /.page-content -->

