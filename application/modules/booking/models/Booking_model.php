<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_model extends CI_Model {

public function gen_status($room, $user_id)
{
	if(!empty($room) && !empty($user_id) )
	{
		//check, if super admin then active
		$phase1 = $this->db->get_where('users_groups', array('user_id' => $user_id, 'group_id' => 1) )->result();
		if(!empty($phase1))
		{
			return 'active';
		}
		
		//check, if admin of room then active
		$phase2 = $this->db->get_where('room', array('admin_id' => $user_id, 'id' => $room) )->result();
		if(!empty($phase2))
		{
			return 'active';
		}

		if( empty($phase1) && empty($phase2) )
		{
			return 'pending';
		}
		
	}
	else
	{
		return 'pending';
	}
}

public function is_allow($event_id, $room_id, $user_id)
{
	//check, if super admin then allow
	$phase1 = $this->db->get_where('users_groups', array('user_id' => $user_id, 'group_id' => 1) )->result();
	if(!empty($phase1))
	{
		return 'true';
	}
	else
	{
		//check, if admin of room then allow
		$phase2 = $this->db->get_where('room', array('admin_id' => $user_id, 'id' => $room_id) )->result();
		if(!empty($phase2))
		{
			return 'true';
		}
		else
		{
			//check, if user of the event then allow
			$phase3 = $this->db->get_where('event', array('user_id' => $user_id, 'id' => $event_id, 'status' => 'pending') )->result();
			// var_dump($phase3);exit;
			if(!empty($phase3))
			{
				return 'true';
			}
			else
			{
				return 'false';
			}
			
		}
		
	}


}


public function is_duplicate_event($id, $start, $area, $room)
{
	if(!empty($id))
	{
		$this->db->where(" id != '$id' ");
	}

	$this->db->where('area_id', $area);
	$this->db->where('room_id', $room);
	$this->db->where(" '$start' BETWEEN start AND end ");
	$this->db->get('event');
	if($this->db->affected_rows() > 0)
	{
		return false;
	}
	else
	{
		return true;
	}

}	

}

/* End of file Booking_model.php */
/* Location: ./application/modules/booking/models/Booking_model.php */