<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends CI_Model {

	public $table;
	public $column_order;
	public $column_search; 
	public $order;
	public $filters; 

	public function __construct()
	{
		parent::__construct();
	}

	private function _get_datatables_query()
	{
		$query = "SELECT
					*, (
						SELECT
							username
						FROM
							mrbs_users a
						WHERE
							a.id = x.user_id
					) AS user_id_name,
					(
						SELECT
							username
						FROM
							mrbs_users a
						WHERE
							a.id = x.admin_id
					) AS admin_id_name,
					(
						SELECT
							NAME
						FROM
							mrbs_area a
						WHERE
							a.id = x.area_id
					) AS area_id_name,
					(
						SELECT
							room_name
						FROM
							mrbs_room a
						WHERE
							a.id = x.room_id
					) AS room_id_name
				FROM
					mrbs_event x ";
		
	
			if($_POST['search']['value']) 
			{
				$query .= " WHERE CONCAT(x.title,x.desc,x.notes,x.qty) like '%".$_POST['search']['value']."%' ";
			}

			if($_POST['event_date'] && !$_POST['search']['value']) 
			{
				$event_date = $this->input->post('event_date');
				$event_date = explode(' - ', $event_date);
				$start 		= $event_date[0];
				$end 		= $event_date[1];

				$query .= " WHERE x.start BETWEEN '".$start."' AND '".$end."' ";
			}
		
		return $query;
	}

	public function get_datatables()
	{
		$this->table 			= 'event';
		$query = $this->_get_datatables_query();

		if($_POST['length'] != -1)
		$query .= "LIMIT ".$_POST['start'].", ".$_POST['length']."";

		$query = $this->db->query($query);
		return $query->result();
	}

	public function count_filtered()
	{
		$query = $this->_get_datatables_query();
		$query = $this->db->query($query);
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}


}

/* End of file datatables_model.php */
/* Location: ./application/models/datatables_model.php */