<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth','form_validation', 'encryption'));
        $this->load->helper(array('language', 'general'));

        $this->load->model('report_model','report');
        $this->load->model('general_model','general');
        $this->load->model('request_model','req');

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');

        if (!$this->ion_auth->logged_in())
        {
            redirect('login-is-required', 'refresh');
        }
    }

    public function index()
    {
    
        if(is_permit('read', 'report'))
        {
            $this->data['message']      = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->data['csrf']         = $this->general->_get_csrf_nonce();
            $this->template
            ->set_partial('js', 'js_index')
            ->build('index', $this->data);
        }
        else
        { 
            redirect('login-is-required', 'refresh');
        }
    
    }

    public function ajax_event_items()
    {
        $list = $this->report->get_datatables();
        $data = array();
        $update = '';
        $delete = '';
        $no = $_POST['start'];
        foreach ($list as $event) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $event->title;
            $row[] = $event->qty;
            $row[] = $event->start;
            $row[] = $event->end;
            $row[] = $event->area_id_name;
            $row[] = $event->user_id_name;
            $row[] = $event->room_id_name;
            $row[] = $event->admin_id_name;
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->report->count_all(),
                        "recordsFiltered" => $this->report->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);

    }


    public function booking_request()
    {
        if(is_permit('read', 'booking_request'))
        {
            $this->data['message']      = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->data['csrf']         = $this->general->_get_csrf_nonce();
            $this->template
            ->set_partial('js', 'js_book_request')
            ->build('book_request', $this->data);
        }
        else
        { 
            redirect('login-is-required', 'refresh');
        }
    }

    public function ajax_event_request()
    {
        $list = $this->req->get_datatables();
        $data = array();
        $update = '';
        $delete = '';
        $no = $_POST['start'];
        foreach ($list as $event) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $event->title;
            $row[] = $event->qty;
            $row[] = $event->start;
            $row[] = $event->end;
            $row[] = $event->area_id_name;
            $row[] = $event->user_id_name;
            $row[] = $event->room_id_name;
            $row[] = $event->admin_id_name;
            $row[] = $event->status;
            if(is_permit('update', 'booking_request') )
            {
                $update = '<a class="btn btn-success" href="" onclick="approve_event(\''.$event->id.'\');return false;">
                                <i class="ace-icon fa fa-check bigger-130"></i> Approve
                            </a>';
           
                $row[] = '  <div class="hidden-sm hidden-xs action-buttons">
                                '.$update.'
                            </div>';
            }
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->req->count_all(),
                        "recordsFiltered" => $this->req->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);

    }

    public function approve_event($id)
    {
        $this->general->update('event', array('id'=>$id), array('status' => 'active') );
        $user_id = $this->db->get_where('event', array('id' => $id) )->row()->user_id;
        
        $from    = 'e-meeting@mail.com';
        $to      = $this->db->get_where('users', array('id' => $user_id) )->row()->email;
        $subject = 'Notification e-meeting room';
        $content = 'Your booking request has been approved';
        if(!empty($to))
        {
            $this->general->send_mail($from, $to, $subject, $content);
        }
        echo 'success';
    }


}

/* End of file Report.php */
/* Location: ./application/modules/report/controllers/Report.php */