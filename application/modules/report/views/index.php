
<div class="page-content" style="background:#FFFFF;">
	<div class="page-header">
		<h1>
			Home
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				Sales Report
			</small>
		</h1>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="panel panel-primary">
				<div class="panel-heading"></div>
				<div class="panel-body">
					<form action="" method="POST" role="form" class="form-horizontal">
					
						<div class="form-group">
							<label for="" class="col-md-2">Date</label>
							<div class="col-md-6">
								<input type="text" id="event_date" class="form-control input-daterange" placeholder="Input Date">
							</div>
							<button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
						</div>
					
						
					
					</form>
				</div>
			</div>

				<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Event Name</th>
							<th>Qty</th>
							<th>Start</th>
							<th>End</th>
							<th>Area</th>
							<th>User</th>
							<th>Room</th>
							<th>Admin</th>
						</tr>
					</thead>
					<tbody>
					</tbody>

					<tfoot>
						<tr>
							<th>No</th>
							<th>Event Name</th>
							<th>Qty</th>
							<th>Start</th>
							<th>End</th>
							<th>Area</th>
							<th>User</th>
							<th>Room</th>
							<th>Admin</th>
						</tr>
					</tfoot>
				</table>
  				
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

  
       