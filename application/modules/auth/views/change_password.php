
<div class="page-content" style="background:#FFFFF;">
	<div class="page-header">
		<h1>
			Home
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				Change Password
			</small>
		</h1>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12 col-md-10">
			<!-- PAGE CONTENT BEGINS -->
			<br>
			<div id="msg" class="text-danger text-center"></div>
			<?=form_open('#', array('id'=>'form', 'class'=>'form-horizontal') ); ?>
        	<?=form_hidden($csrf); ?>
            <input type="hidden" value="" name="id"/> 
            <div class="form-body">
                <div class="form-group">
					<label for="username" class="col-md-3 control-label">User Name</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="username">
						<span class="help-block"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="username" class="col-md-3 control-label">Password</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="password">
						<span class="help-block"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-md-3 control-label">Email</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="email">
						<span class="help-block"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="first_name" class="col-md-3 control-label">First Name</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="first_name">
						<span class="help-block"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="last_name" class="col-md-3 control-label">Last Name</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="last_name">
						<span class="help-block"></span>
					</div>
				</div>
				
				<div class="form-group">
					<label for="LastName" class="col-md-3 control-label">Phone</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="phone">
						<span class="help-block"></span>
					</div>
				</div>
                <div class="form-group" id="photo-preview">
                    <label class="control-label col-md-3">Photo</label>
                    <div class="col-md-9">
                        (No photo)
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3" id="label-photo">Upload Photo </label>
                    <div class="col-md-9">
                        <input name="photo" type="file">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-md-10 col-xs-12 text-center">
                		  <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
				          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                	</div>
                </div>
            </div>
        <?=form_close();?>

		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->



