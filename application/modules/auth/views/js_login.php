<script>

$(function() {
	$('#f_reset').hide();	  
});

function reset_pass()
{
	$('#f_login').hide();	
	$('#f_reset').show();	
}

function cancel_reset()
{
	$('#f_login').show();	
	$('#f_reset').hide();	
}

</script>
<script type="text/javascript">
    $(function() {
        var date = new Date();
        
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        var data_events = [];
        reload_calendar(data_events);

        $('[name="area_id"]').change(function(event) {
            var area = $(this).val();
            $.post("<?=site_url('auth/get_rooms')?>", {area: area}, function(resp, textStatus, xhr) {
                $('[name="room_id"]').html(resp);
                $('[name="area_id"]').val(area);
            });
        });

        $('[name="room_id"]').change(function(event) {
                var room = $(this).val();
                $('[name="room_id"]').val(room);
        });

        $('#form-filter').submit(function(event) {
            event.preventDefault();
            $('#btn-filter').addClass('disabled');
            $('#btn-filter').text('Loading data..');
            var data_post = $(this).serialize();
            $.getJSON("<?=site_url('auth/get_events')?>", data_post, function(json, textStatus) {
                $('#btn-filter').removeClass('disabled');
                $('#btn-filter').text('Filter');
                reload_calendar(json);

            });

            
        });

        //daterangepicker
         $('.input-daterange').daterangepicker({
            'minDate':moment(),
            'parentEl': "#modal-form",
            'autoUpdateInput': false,
            'applyClass' : 'btn-sm btn-success',
            'cancelClass' : 'btn-sm btn-default',
            "timePicker": true,
            "timePicker24Hour": true,
            locale: {
                applyLabel  : 'Apply',
                cancelLabel : 'Cancel',
            }
        })

        $('.input-daterange').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('YYYY-MM-DD h:mm:ss') + ' - ' + picker.endDate.format('YYYY-MM-DD h:mm:ss'));
        });

        $('.input-daterange').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
        });


    });

    function reload_calendar(data_events) 
    {
        $('#calendar').fullCalendar('destroy');

        var calendar = $('#calendar').fullCalendar({
            buttonHtml: {
                prev: '<i class="ace-icon fa fa-chevron-left"></i>',
                next: '<i class="ace-icon fa fa-chevron-right"></i>'
            },
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            events: data_events,
            editable: true,
            selectable: false,
            eventClick: function(calEvent, jsEvent, view) {
                edit_event(calEvent.id)
        }
    });


    }


    function reset_form()
    {
        $('[name="title"]').val('');
        $('[name="event_date"]').val('');
        $('[name="desc"]').val('');
        $('[name="qty"]').val('');
        $('[name="notes"]').val('');
        $('[name="qty"]').val('');
    }

    function edit_event(id)
    {
        save_method = 'update';
        reset_form()
        // $('#form-event')[0].reset(); // reset form on modals
        $('#msg').html('');
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        $.ajax({
            url : "<?php echo site_url('auth/ajax_edit_event')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('.area_id').text( $('[name="area_id"]').find(":selected").text() );
                $('.room_id').text( $('[name="room_id"]').find(":selected").text() );
                $('.title').text(data.title);
                $('.event_date').text(data.event_date)
                $('.desc').text(data.desc)
                $('.qty').text(data.qty)
                $('.notes').text(data.notes)
                $('.qty').text(data.qty)
                $('#modal-form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Event'); // Set title to Bootstrap modal title
                
                $('[name="room_id"]').val(data.room_id);

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function show_login()
    {
        $('#login-box').toggle(500);
    }


</script>