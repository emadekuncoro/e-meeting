
<div class="page-content" style="background:#FFFFF;">
	<div class="page-header">
		<h1>
			Home
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				<a href="<?=site_url('auth/group')?>">Groups</a>
			</small>
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				Usergroup Management
			</small>
		</h1>
	</div><!-- /.page-header -->
	
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			 <h3>Users Data</h3>
			 
				<br>
				<div class="table-header">
					Group Name : <?=$group_name;?>
				</div>
				<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
						
							<th>No</th>
							<th>Username</th>
							<th>Email</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Phone</th>
							<th>Photo</th>
							<th>-</th>
						
						</tr>
					</thead>
					<tbody>
					</tbody>

					<tfoot>
						<tr>
							<th>No</th>
							<th>Username</th>
							<th>Email</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Phone</th>
							<th>Photo</th>
							<th>-</th>

						</tr>
					</tfoot>
				</table>
  				<!-- form input/edit -->
				<hr>


			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

  
       