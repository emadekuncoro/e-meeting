<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth','form_validation', 'encryption'));
		$this->load->helper(array('language', 'general'));

		$this->load->model('datatables_model','datatables');
		$this->load->model('general_model','general');
        $this->load->model(array('role_model', 'auth_model') );

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

		if (!$this->ion_auth->logged_in())
		{
            // redirect them to the login page
			redirect('login-is-required', 'refresh');
		}
	}

	public function index()
	{
		if(is_permit('read', 'group'))
		{
			$this->data['csrf'] 	= $this->general->_get_csrf_nonce();
            $this->template
            ->set_partial('js', 'js_group')
            ->build('group', $this->data);
    	}
    	else
    	{
    		redirect('auth/logout','refresh');
    	}
	}

    public function ajax_list_group()
    {
		$table 			= 'groups';
		$column_order 	= array(null, 'name', 'description'); 
		$column_search 	= array('name', 'description');  
		$order 			= array('id' => 'asc'); // default order 

        $list = $this->datatables->get_datatables($table, $column_order, $column_search, $order);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $group_model) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $group_model->name;
            $row[] = $group_model->description;
            
            if(is_permit('update', 'group') || is_permit('delete', 'group') )
			{
				if(is_permit('update', 'group'))
				{
					$update = '<a title="user list" class="blue" href="'.site_url('auth/group/user_group').'/'.base64url_encode($group_model->id).'" >
								<i class="ace-icon fa fa-user bigger-130"></i>
								</a>
								<a title="menu list" class="red" href="'.site_url('auth/group/menu_group').'/'.base64url_encode($group_model->id).'" >
									<i class="ace-icon fa fa-tags bigger-130"></i>
								</a>
								<a class="green" href="#" onclick="edit_group(\''.$group_model->id.'\')">
									<i class="ace-icon fa fa-pencil bigger-130"></i>
								</a>';
				}
				
				if(is_permit('delete', 'group'))
				{
					$delete = '<a class="red" href="#" onclick="delete_group(\''.$group_model->id.'\')">
								<i class="ace-icon fa fa-trash-o bigger-130"></i>
								</a>';
				}

	            $row[] = '	<div class="hidden-sm hidden-xs action-buttons">
								'.$update.$delete.'
							</div>';
			}


            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->datatables->count_all(),
                        "recordsFiltered" => $this->datatables->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    //ajax edit group
	public function ajax_edit_group($id)
	{
		$data = $this->general->get_by_id('groups', $id);
		echo json_encode($data);
	}
	//ajax add group
	public function ajax_add_group()
	{
		$this->_validate_group();
		$name 	= $this->input->post('name');
		$desc 	= $this->input->post('description');
		
		$this->ion_auth->create_group($name, $desc);
		echo json_encode(array("status" => TRUE));
	}

	//ajax update group
	public function ajax_update_group()
	{
		$this->_validate_group();
       	$this->ion_auth->ion_auth->update_group($this->input->post('id'), $this->input->post('name'), $this->input->post('description'));

		echo json_encode(array("status" => TRUE));
	}

	//ajax delete group
	public function ajax_delete_group($id)
	{
		$this->auth_model->delete_Groupby_id($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate_group()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name') == '')
		{
			$data['inputerror'][] = 'name';
			$data['error_string'][] = 'Name is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('description') == '')
		{
			$data['inputerror'][] = 'description';
			$data['error_string'][] = 'Description is required';
			$data['status'] = FALSE;
		}

		if ($this->general->_valid_csrf_nonce() === FALSE )
		{
			$data['inputerror'][] = 'msg';
			$data['error_string'][] = 'Go Home Kids';
			$data['status'] = FALSE;
		}
			
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	//user group
	public function user_group()
	{
		$id 						= $this->general->decode_uri();
		// var_dump($id);exit;
		if(empty($id)) //redirect to home if id = null
			redirect('root','refresh');

		$this->data['id']			= base64url_encode($id);
		$this->data['csrf'] 		= $this->general->_get_csrf_nonce();
		$this->data['group_name'] 	= $this->general->get_by_id('groups', $id)->description;
        $this->template
        ->set_partial('js', 'js_user_group')
        ->build('user_group', $this->data);
	
	}

	public function ajax_list_usergroup()
    {
    	$group_id  		= base64url_decode($this->input->post('uri'));

    	$table 			= 'users';
		$column_order 	= array(null, 'username', 'email', 'first_name','last_name','phone',null);   
		$column_search 	= array('username', 'email', 'first_name','last_name','phone'); 
		$filters     	= array('username', 'email', 'first_name','last_name','phone');
		$order 			= array('id' => 'asc'); // default order 

        $list 			= $this->datatables->get_datatables($table, $column_order, $column_search, $order, $filters);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $user) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $user->username;
            $row[] = $user->email;
            $row[] = $user->first_name;
            $row[] = $user->last_name;
            $row[] = $user->phone;
            if($user->photo)
            {
				$row[] = '<a href="'.base_url('upload/user/'.$user->photo).'" target="_blank"><img src="'.base_url('upload/user/'.$user->photo).'" class="img-responsive" width="70px" /></a>';
            }
			else
			{
				$row[] = '(No photo)';
			}

			if($this->auth_model->check_usergroup($user->id, $group_id))
			{
				$state = 'checked';
			}
			else
			{
				$state = '';
			}

            $row[] = '<label>
						<input '.$state.' onchange="save()" class="data-check ace ace-switch" value="'.$user->id.'" type="checkbox" />
						<span class="lbl"></span>
					  </label>';

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->datatables->count_all(),
                        "recordsFiltered" => $this->datatables->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_update_usergroup()
    {
    	$user_id 	= $this->input->post('id');
    	$group_id 	= base64url_decode($this->input->post('uri'));

    	$this->db->delete('users_groups', array('group_id' => $group_id) );
    	if(!empty($user_id))
    	{
	    	foreach ($user_id as $value) 
	    	{
	    		$this->auth_model->update_usergroup($value, $group_id);
	    	}
    	}

    	echo json_encode(array("status" => TRUE));
    }

    //menu group
    public function menu_group()
	{
		$id 						= $this->general->decode_uri();
		if(empty($id)) //redirect to home if id = null
			redirect('root','refresh');

		$this->data['id'] 			= base64url_encode($id);
		$this->data['csrf'] 		= $this->general->_get_csrf_nonce();
		$this->data['group_name'] 	= $this->general->get_by_id('groups', $id)->description;
        $this->template
        ->set_partial('js', 'js_menu_group')
        ->build('menu_group', $this->data);
	
	}

	//ajax list menu_group
    public function ajax_list_menugroup()
    {
        $list = $this->role_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $menu) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $menu->name;
            $row[] = $menu->description;
            $row[] = $menu->access;
            $row[] = '	<div class="hidden-sm hidden-xs action-buttons">
							<a class="green" href="#" onclick="edit_menu(\''.$menu->id.'\')">
								<i class="ace-icon fa fa-pencil bigger-130"></i>
							</a>
							<a class="red" href="#" onclick="delete_menu(\''.$menu->id.'\')">
								<i class="ace-icon fa fa-trash-o bigger-130"></i>
							</a>
						</div>';

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->role_model->count_all(),
                        "recordsFiltered" => $this->role_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_add_menugroup()
	{
		$this->_validate_menugroup();
		$data 	= array(
				'menu_id' 		=> $this->input->post('menu_id'),
				'group_id' 		=> base64url_decode($this->input->post('uri')),
				'access' 	=> implode('', $this->input->post('access')),
				'state' 	=> 'open',
			);

		$insert = $this->general->save('menu_access', $data);
		echo json_encode(array("status" => TRUE));
	}
	
	//ajax edit menu
	public function ajax_edit_menugroup($id)
	{
		$data = $this->general->get_by_id('menu_access', $id);
		$data->crud = array();
		if(!empty($data->access))
		{
			$crud = array('c', 'r', 'u', 'd');
			foreach ($crud as $val) 
			{
				$check = strpos($data->access, $val);
				if ($check !== false) 
				{
					$data->crud[] = $val; 
				}
			}
			
		}
		echo json_encode($data);
	}

	//ajax update group
	public function ajax_update_menugroup()
	{
		$this->_validate_menugroup();
		$data 	= array(
				'menu_id' 		=> $this->input->post('menu_id'),
				'group_id' 		=> base64url_decode($this->input->post('uri')),
				'access' 		=> implode('', $this->input->post('access')),
				'state' 		=> 'open',
			);

		$this->general->update('menu_access', array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => TRUE));
	}
	//ajax delete group
	public function ajax_delete_menugroup($id)
	{
		$this->general->delete_by_id('menu_access', $id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate_menugroup()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('menu_id') == '')
		{
			$data['inputerror'][] = 'menu_id';
			$data['error_string'][] = 'Menu is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('access') == '')
		{
			$data['inputerror'][] = 'access';
			$data['error_string'][] = 'Access is required';
			$data['status'] = FALSE;
		}

		if ($this->general->_valid_csrf_nonce() === FALSE )
		{
			$data['inputerror'][] = 'msg';
			$data['error_string'][] = 'Go Home Kids';
			$data['status'] = FALSE;
		}
			
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


}

/* End of file Group.php */
/* Location: ./application/modules/auth/controllers/Group.php */