<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth','form_validation', 'encryption'));
		$this->load->helper(array('language', 'general'));

		$this->load->model('datatables_model','datatables');
		$this->load->model('general_model','general');
        $this->load->model(array('role_model', 'auth_model') );

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

		if (!$this->ion_auth->logged_in())
		{
            // redirect them to the login page
			redirect('login-is-required', 'refresh');
		}
	}

	public function index()
	{
	
		if(is_permit('read', 'user'))
		{
			$this->data['message'] 		= (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['csrf'] 		= $this->general->_get_csrf_nonce();
			$this->data['menu_header'] 	= $this->db->get('menu')->result();
            $this->template
            // ->set_layout('admin') 
            ->set_partial('js', 'js_index')
            ->build('index', $this->data);
		}
		else
		{ 
			redirect('login-is-required', 'refresh');
		}
	
	}

    //ajax list user
    public function ajax_list_user()
    {

        $table 			= 'users';
		$column_order 	= array(null, 'username', 'email', 'first_name','last_name','phone',null);   
		$column_search 	= array('username', 'email', 'first_name','last_name','phone'); 
		$filters     	= array('username', 'email', 'first_name','last_name','phone');
		$order 			= array('id' => 'asc'); // default order 

        $list 	= $this->datatables->get_datatables($table, $column_order, $column_search, $order, $filters);
        $data 	= array();
        $update = '';
        $delete = '';
        $no = $_POST['start'];
        foreach ($list as $user) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $user->username;
            $row[] = $user->email;
            $row[] = $user->first_name;
            $row[] = $user->last_name;
            $row[] = $user->phone;
            if($user->photo)
            {
				$row[] = '<a href="'.base_url('upload/user/'.$user->photo).'" target="_blank"><img src="'.base_url('upload/user/'.$user->photo).'" class="img-responsive" width="70px" /></a>';
            }
			else
			{
				$row[] = '(No photo)';
			}


			if(is_permit('update', 'user') || is_permit('delete', 'user') )
			{
				if(is_permit('update', 'user'))
				{
					$update = '<a class="green" href="#" onclick="edit_user(\''.$user->id.'\')">
									<i class="ace-icon fa fa-pencil bigger-130"></i>
								</a>';
				}
				
				if(is_permit('delete', 'user'))
				{
					$delete = '<a class="red" href="#" onclick="delete_user(\''.$user->id.'\')">
									<i class="ace-icon fa fa-trash-o bigger-130"></i>
								</a>';
				}

	            $row[] = '	<div class="hidden-sm hidden-xs action-buttons">
								'.$update.$delete.'
							</div>';
			}

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->datatables->count_all(),
                        "recordsFiltered" => $this->datatables->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    //ajax edit user
	public function ajax_edit_user($id=null)
	{
		if(empty($id))
		{
			$id = $this->session->userdata('user_id');
		}

		$data = $this->general->get_by_id('users', $id);
		echo json_encode($data);
	}
	//ajax add user
	public function ajax_add_user()
	{
		$this->_validate('add');
		$identity 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$email 		= $this->input->post('email');
		$data 		= array(
						'first_name' => $this->input->post('first_name'),
						'last_name' => $this->input->post('last_name'),
						'phone' => $this->input->post('phone'),
					);

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->general->_do_upload('upload/user');
			$data['photo'] = $upload;
		}

		// $insert = $this->user->save($data);
		if($this->ion_auth->register($identity, $password, $email, $data))
		{
			echo json_encode(array("status" => TRUE));
		}
		else
		{
			echo json_encode(array("status" => FALSE, "msg" => "Dupllicate username or email address"));
		}
	}
	//ajax update user
	public function ajax_update_user()
	{
		$action = '';
		if ($this->input->post('password'))
        {
            $action = 'add';
        }
		$this->_validate($action);
		$data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'username' => $this->input->post('username'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
			);

		 // update the password if it was posted
        if ($this->input->post('password'))
        {
            $data['password'] = $this->input->post('password');
        }

		if($this->input->post('remove_photo')) // if remove photo checked
		{
			if(file_exists('upload/user/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink('upload/user/'.$this->input->post('remove_photo'));
			$data['photo'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->general->_do_upload('upload/user');
			
			//delete file
			$user = $this->general->get_by_id('users', $this->input->post('id'));
			if(file_exists('upload/user/'.$user->photo) && $user->photo)
				unlink('upload/user/'.$user->photo);

			$data['photo'] = $upload;
		}
		
		if($this->ion_auth->update($this->input->post('id'), $data))
		{
			echo json_encode(array("status" => TRUE));
		}
		else
		{
			echo json_encode(array("status" => FALSE, "msg" => "Dupllicate username or email address"));
		}

	}

	//ajax delete user
	public function ajax_delete_user($id)
	{
		//delete file
		$user = $this->general->get_by_id('users', $id);
		// var_dump($this->db->last_query());return;
		if(file_exists('upload/user/'.$user->photo) && $user->photo)
			unlink('upload/user/'.$user->photo);
		
		$this->auth_model->delete_Userby_id($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($action=null)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('first_name') == '')
		{
			$data['inputerror'][] = 'first_name';
			$data['error_string'][] = 'First name is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('last_name') == '')
		{
			$data['inputerror'][] = 'last_name';
			$data['error_string'][] = 'Last name is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('username') == '')
		{
			$data['inputerror'][] = 'username';
			$data['error_string'][] = 'Username is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('password') == '' && $action == 'add')
		{
			$data['inputerror'][] = 'password';
			$data['error_string'][] = 'Password is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('email') == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Email is required';
			$data['status'] = FALSE;
		}

		if ($this->general->_valid_csrf_nonce() === FALSE )
		{
			$data['inputerror'][] = 'msg';
			$data['error_string'][] = 'Go Home Kids | reload pages';
			$data['status'] = FALSE;
		}
			
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}    


}

/* End of file User.php */
/* Location: ./application/modules/auth/controllers/User.php */