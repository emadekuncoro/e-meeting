<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function generate_permission($groups)
	{
		$this->db->select('b.code,a.*');
		$this->db->from('menu_access a');
		$this->db->join('menu b', 'a.menu_id = b.id', 'left');
		$this->db->where_in('a.group_id', $groups);
		$this->db->group_by('a.menu_id');
		$res = $this->db->get()->result();
		$perm = array();
		foreach ($res as $value) 
		{
			if(!empty($value->access))
			{
				$perm[] = $value->access.'|=|=|'.$value->code;
			}
		}

		
		$this->session->set_userdata('permission', $perm );
	}

	public function delete_Groupby_id($id)
	{
		//delete data on menu_access table
		$this->db->where('group_id', $id);
		$this->db->delete('menu_access');
		//delete data on user_groups table
		$this->db->where('group_id', $id);
		$this->db->delete('users_groups');
		//delete data group
		$this->db->where('id', $id);
		$this->db->delete('groups');
	}

	public function delete_Menuby_id($id)
	{
		//delete data on menu_access table
		$this->db->where('menu_id', $id);
		$this->db->delete('menu_access');
	
		//delete data menu
		$this->db->where('id', $id);
		$this->db->delete('menu');
	}
	
	
	public function delete_Userby_id($id)
	{
		//delete data on user_groups table
		$this->db->where('user_id', $id);
		$this->db->delete('users_groups');
		//delete data user
		$this->db->where('id', $id);
		$this->db->delete('users');
	}

	public function get_menu_byGroup($group_id, $edit_id=null)
	{
		$res = array();
		$this->db->distinct();
		$list_id = $this->db->get_where('menu_access', array('group_id'=>$group_id) )->result();
		if(count($list_id) > 0)
		{
			foreach ($list_id as $value) 
			{
				$res[] = $value->menu_id;
			}

		}
		
		if(!empty($edit_id) && count($res) > 1) //edit menu group
		{
			$edit = $this->db->get_where('menu_access', array('id'=>$edit_id) )->row()->menu_id;
			$res  = array_diff($res, [0 => $edit] );

			return $this->db
					->where_not_in('id', $res)
					->get('menu')->result();
		}
		elseif(empty($edit_id) && count($res) > 0) //add menu group
		{
			return $this->db
					->where_not_in('id', $res)
					->get('menu')->result();

		}
		else //list all menu (default)
		{
			return $this->db->get('menu')->result();
		}
			 
		
	}	

	public function check_usergroup($id, $group_id)
	{
		$check = $this->db->get_where('users_groups', array('user_id' => $id, 'group_id' => $group_id) )->row();
		return $res = count($check) == 0 ? false : true;
	}

	public function update_usergroup($id, $group_id)
	{
		$check = $this->db->get_where('users_groups', array('user_id' => $id, 'group_id' => $group_id) )->row();
		if(count($check) == 0 )
		{
			$this->db->insert('users_groups', array('user_id' => $id, 'group_id' => $group_id) );
		}

	}


}
