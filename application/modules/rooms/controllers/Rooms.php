<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rooms extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth','form_validation', 'encryption'));
		$this->load->helper(array('language', 'general'));

		$this->load->model('datatables_model','datatables');
		$this->load->model('general_model','general');
		$this->load->model('rooms_model','room');

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

		if (!$this->ion_auth->logged_in())
		{
			redirect('login-is-required', 'refresh');
		}
	}
	
	public function index()
	{
		
		if(is_permit('read', 'rooms'))
		{
			$this->data['message'] 		= (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['csrf'] 		= $this->general->_get_csrf_nonce();
			$this->data['areas'] 		= $this->db->get('area')->result();
			$this->data['admins'] 		= $this->room->get_admin();

            $this->template
            ->set_partial('js', 'js_index')
            ->build('index', $this->data);
		}
		else
		{ 
			redirect('login-is-required', 'refresh');
		}
	
	}

    //ajax list area
    public function ajax_list_room()
    {

        $table 			= 'room';
		$column_order 	= array(null, 'room_name', 'address', 'capacity');   
		$column_search 	= array('room_name', 'address', 'capacity'); 
		$filters     	= array('room_name', 'address', 'capacity');
		$order 			= array('room_name' => 'asc'); // default order 

        $list 	= $this->datatables->get_datatables($table, $column_order, $column_search, $order, $filters);
        $data 	= array();
        $update = '';
        $delete = '';
        $no = $_POST['start'];
        foreach ($list as $room) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $room->room_name;
            $row[] = $this->general->get_by_id('area', $room->area_id)->name;
            $row[] = $room->floor;
            $row[] = $room->capacity;
            $row[] = $this->general->get_by_id('users', $room->admin_id)->username;

            if($room->photo)
            {
				$row[] = '<a href="'.base_url('upload/room/'.$room->photo).'" target="_blank"><img src="'.base_url('upload/room/'.$room->photo).'" class="img-responsive" width="70px" /></a>';
            }
			else
			{
				$row[] = '(No photo)';
			}

			if(is_permit('update', 'rooms') || is_permit('delete', 'rooms') )
			{
				if(is_permit('update', 'rooms'))
				{
					$update = '<a class="green" href="#" onclick="edit_room(\''.$room->id.'\')">
									<i class="ace-icon fa fa-pencil bigger-130"></i>
								</a>';
				}
				
				if(is_permit('delete', 'rooms'))
				{
					$delete = '<a class="red" href="#" onclick="delete_room(\''.$room->id.'\')">
									<i class="ace-icon fa fa-trash-o bigger-130"></i>
								</a>';
				}

	            $row[] = '	<div class="hidden-sm hidden-xs action-buttons">
								'.$update.$delete.'
							</div>';
			}

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->datatables->count_all(),
                        "recordsFiltered" => $this->datatables->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_add_room()
	{
		$this->_validate();
		$data 	= array(
				'room_name' 	=> $this->input->post('room_name'),
				'area_id' 		=> $this->input->post('area_id'),
				'admin_id' 		=> $this->input->post('admin_id'),
				'address' 		=> $this->input->post('address'),
				'floor' 		=> $this->input->post('floor'),
				'capacity' 		=> $this->input->post('capacity'),
				'facility' 		=> $this->input->post('facility'),
				'status' 		=> $this->input->post('status'),
			);

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->general->_do_upload('upload/room');
			$data['photo'] = $upload;
		}

		$insert = $this->general->save('room', $data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_edit_room($id)
	{
		$data = $this->general->get_by_id('room', $id);
		echo json_encode($data);
	}

	public function ajax_update_room()
	{
		$this->_validate();
		$data 	= array(
				'room_name' 	=> $this->input->post('room_name'),
				'area_id' 		=> $this->input->post('area_id'),
				'admin_id' 		=> $this->input->post('admin_id'),
				'address' 		=> $this->input->post('address'),
				'floor' 		=> $this->input->post('floor'),
				'capacity' 		=> $this->input->post('capacity'),
				'facility' 		=> $this->input->post('facility'),
				'status' 		=> $this->input->post('status'),
			);

		if($this->input->post('remove_photo')) // if remove photo checked
		{
			if(file_exists('upload/room/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink('upload/room/'.$this->input->post('remove_photo'));
			$data['photo'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->general->_do_upload('upload/room');
			
			//delete file
			$room = $this->general->get_by_id('room', $this->input->post('id'));
			if(file_exists('upload/room/'.$room->photo) && $room->photo)
				unlink('upload/room/'.$room->photo);

			$data['photo'] = $upload;
		}
		

		$this->general->update('room', array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete_room($id)
	{
		$this->general->delete_by_id('room', $id);
		echo json_encode(array("status" => TRUE));
	}  

	private function _validate($action=null)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('room_name') == '')
		{
			$data['inputerror'][] = 'room_name';
			$data['error_string'][] = 'Room name is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('area_id') == '')
		{
			$data['inputerror'][] = 'area_id';
			$data['error_string'][] = 'Area is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('capacity') == '')
		{
			$data['inputerror'][] = 'capacity';
			$data['error_string'][] = 'Capacity is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('admin_id') == '')
		{
			$data['inputerror'][] = 'admin_id';
			$data['error_string'][] = 'Admin is required';
			$data['status'] = FALSE;
		}

		if ($this->general->_valid_csrf_nonce() === FALSE )
		{
			$data['inputerror'][] = 'msg';
			$data['error_string'][] = 'Go Home Kids | reload pages';
			$data['status'] = FALSE;
		}
			
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}    


}

/* End of file Areas.php */
/* Location: ./application/modules/areas/controllers/Areas.php */