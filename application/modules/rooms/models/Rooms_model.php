<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rooms_model extends CI_Model {

	public function get_admin()
	{
		$this->db->select('a.id,a.username');
		$this->db->from('users a');
		$this->db->join('users_groups b', 'a.id = b.user_id', 'left');
		$this->db->where('b.group_id', 2);
		$data = $this->db->get();
		return $data->result();
	}
		

}

/* End of file Rooms_model.php */
/* Location: ./application/modules/rooms/models/Rooms_model.php */